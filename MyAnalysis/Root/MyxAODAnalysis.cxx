#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>

#include <xAODEventInfo/EventInfo.h>

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"


#include <xAODCaloEvent/CaloClusterContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODTracking/TrackParticleContainer.h>




MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0,
                   "Minimum electron pT (in MeV)" );
  declareProperty( "SampleName", m_sampleName = "ee",
                   "Descriptive name for the processed sample" );

  declareProperty( "JetnPtCut", m_jetPtCut = 20000.0,
                   "Minimum jet pT (in MeV)" );
}



StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ANA_MSG_INFO(" I am algorithm: " <<name());

  ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]
  ANA_CHECK (book (TH1F ("h_jetEta", "h_jetEta", 100, -5, 5))); // jet eta
  ANA_CHECK (book (TH1F ("h_jetPhi", "h_jetPhi", 100, -7, 7))); // jet phi
  ANA_CHECK (book (TH1F ("h_jetE", "h_jetE", 100, 0, 500))); // jet E [GeV]
  double eta_bins_edges[] = {-2.47, -2.37, -2.01, -1.81, -1.52, -1.37, -1.15, -0.80,
                    -0.60, -0.10, 0.00, 0.10, 0.60, 0.80, 1.15, 1.37, 1.52,
                    1.81, 2.01, 2.37, 2.47};
  double pt_bins_edges[] = {0.,15.,20.,25.,30.,35.,40.,45.,50.,60.,80.,150.,250.,10000.};
  ANA_CHECK (book (TH2F ("truthPromptElectronAll", "truthPromptElectronAll", 13, pt_bins_edges, 20,eta_bins_edges))); // prompt electron pt, eta
  ANA_CHECK (book (TH2F ("promptElectronAll", "promptElectronAll", 13, pt_bins_edges, 20,eta_bins_edges))); // prompt electron pt, eta

  m_firstEvent = true;
  
  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.


  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // print out run and event number from retrieved object
  ANA_MSG_INFO ("alg "<<name()<<": in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  const xAOD::CaloClusterContainer* cls = nullptr;
  ANA_CHECK (evtStore()->retrieve ( cls, "egammaClusters" )); 
  for (const xAOD::CaloCluster* cl : *cls) {
    double moment = 0.0;
    cl->retrieveMoment( xAOD::CaloCluster::PHICALOFRAME, moment );
  }

  // get jet container of interest
  const xAOD::JetContainer* jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (jets, "AntiKt4EMPFlowJets"));
  ANA_MSG_INFO ("execute(): number of jets = " << jets->size());
  // loop over the jets in the container
  for (const xAOD::Jet *jet : *jets) {
    //ANA_MSG_INFO ("execute(): jet pt = " << (jet->pt() * 0.001) << " GeV"); // just to print out something
    hist ("h_jetPt")->Fill (jet->pt() * 0.001); // GeV
    hist ("h_jetEta")->Fill (jet->eta() ); // eta
    hist ("h_jetPhi")->Fill (jet->phi() ); // phi
    hist ("h_jetE")->Fill (jet->e() * 0.001); // GeV
  } // end for loop over jets

  // const xAOD::TruthParticleContainer* truthParticles = nullptr;
  // ANA_CHECK (evtStore()->retrieve ( truthParticles, "TruthParticleContainer" )); 


  // ATH_MSG_DEBUG("------------ Truth Particles Container ---------------");
  // unsigned int promtpElectronTruthIndex = -9;
  // for (const auto *truth : *truthParticles)
  // {

  //   if (!truth)
  //     continue;
  //   if (fabs(truth->pdgId()) != 11)
  //     continue;

  //   auto res = m_mcTruthClassifier->particleTruthClassifier(truth);
  //   MCTruthPartClassifier::ParticleOrigin TO = res.second;
  //   MCTruthPartClassifier::ParticleType TT = res.first;
  //   if (m_firstEvent)
  //     ATH_MSG_INFO(" ******** Truth particle associated to Electron Found: "
  //                   << " STATUS  " << truth->status()
  //                   << " type  " << truth->type()
  //                   << " barcode  " << truth->barcode()
  //                   << " PDG id   " << truth->pdgId()
  //                   << " index    " << truth->index()
  //                   << " TO  " << TO
  //                   << " TT   " << TT
  //                   << " eventNumber  " << eventInfo->eventNumber());

  //   // Check if it is the prompt electron
  //   if (TO == MCTruthPartClassifier::SingleElec &&
  //       TT == MCTruthPartClassifier::IsoElectron && truth->barcode() == 10001)
  //   {
  //     hist ("truthPromptElectronAll")->Fill(1.0);
  //     //promtpElectronTruthIndex = truth->index();
  //   }

  //   // Check that it is not from geant4
  //   if (TT != MCTruthPartClassifier::NonPrimary)
  //     hist( "truthElectronAll")->Fill(1.0);
  // }

  // get track container of interest
  const xAOD::TrackParticleContainer* tracks = nullptr;
  ANA_CHECK (evtStore()->retrieve (tracks, "InDetTrackParticles"));
  ANA_MSG_INFO ("execute(): number of tracks = " << tracks->size());
  // loop over the tracks in the container
  for (const xAOD::TrackParticle *track : *tracks) {
    if (track->pt()>10000.0)
      ANA_MSG_INFO ("execute(): track pt = " << (track->pt() * 0.001) << " GeV"); // just to print out something
  } // end for loop over tracks


  if (m_firstEvent)
    m_firstEvent = false;

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}